import "./App.css";
import { NavbarUI } from "./components/NavbarUI";
import {ButtonLogin} from "./components/common/ButtonLogin"

function App() {
  return (
    <>
      <div className="App">
        <NavbarUI />
        <main className="content">
          <h1>SISTEMA DE PUNTO DE VENTA - POS</h1>
          <p>
            La Droguería Jose A, es una empresa familiar fundada hace
            aproximadamente 38 años, <br />
            por la pareja de esposos Anais Lovera y José Antonio Cabezas.
          </p>
          <div className="buttons__container">
            <ButtonLogin text={'Contacto'} />
            <ButtonLogin text={'Ingresar'} />
          </div>
        </main>
      </div>
    </>
  );
}

export default App;
