import { Nav } from './common/Nav';
// import { SocialBottons } from './common/SocialBottons';
import { ButtonLogin } from './common/ButtonLogin';
import './NavbarUI.css';
import logo from '../assets/img/logo.svg';

export const NavbarUI = () => {
  return(
    <header className="navbar__container">
      <img src={logo} className="navbar__container-logo" alt="Logo" />
      <Nav />
      {/* <SocialBottons /> */}
      <ButtonLogin text={'Ingresar'} />
    </header>
  );
}