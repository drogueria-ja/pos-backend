import "./Nav.css";

export const Nav = () => {
  return (
    <ul>
      <li>Inicio</li>
      <li>Documentación</li>
      <li>Nosotros</li>
    </ul>
  );
}