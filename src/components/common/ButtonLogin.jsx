import "./ButtonLogin.css";

export const ButtonLogin = ({ text }) => {
  return (
    <button type="button">{text}</button>
  );
}