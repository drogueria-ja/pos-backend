Summary 

(Give a summary of the issue) 

Steps to reproduce 

(Indicates the steps to reproduce the bugs)

What's the current behavior?

What's the expected behavior? 